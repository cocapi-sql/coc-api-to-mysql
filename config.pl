#! /usr/bin/perl

use strict;
use warnings;

##########
# SQL Variables
##########
# set to sql platform
sub get_sql_platform {
 	return "mysql";
};
# set to database name
sub get_dbname {
 	return "<DB NAME>";
};
# set to database location
sub get_servername {
	return "localhost";
};
# set to database username
sub get_username {
	return "<DB USERNAME>";
};
# set to database password
sub get_password {
	return '<DB PASSWORD>';
};

##########
# API Token
##########
# Two API Tokens are needed. One will be used to pull the Clan data, while a different
# token will be used to pull member data. 
#
# Get these API Tokens from https://developer.clashofclans.com/#/getting-started
#
# set to Clan API Token
sub get_api_clan_token {
	return "<CLAN API TOKEN>";		#set to COC API key
};
# set to Member API Token
sub get_api_member_token {
	return "<MEMBER API TOKEN>";
};

##########
# Clan List
##########
# set to Clan Tag List
sub get_clan_list {
	my @clan_tags = ();
	push @clan_tags, '<CLAN TAG>';
	
	# To include multiple clans use the line below
	#push @clan_tags, '<CLAN TAG>';
	return @clan_tags;
};

##########
# API Token
##########
# set to Debug (verbose logging)
#	0 = debug off
#	1 = debug on (debug mode)
sub get_debug {
	return 0
};
# set to Test (SQL Insert)
#	0 = SQL Insert on
#	1 = SQL Insert off (test mode)
sub get_test {
	return 0
};


return 1;

