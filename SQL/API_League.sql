-----------------------------------------------
--                  COC API                  -- 
--                Version 1.1                --
--                                           --
--     Used to pull Clash of Clans clan,     --
--    member and war log data via the API    --
--      and load this data into mySQL.       --
--                                           --
--              by Gary Douglas              --
--               Oct. 16, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Table structure for table `API_League`
--

CREATE TABLE IF NOT EXISTS `API_League` (
  `league_id` int(10) NOT NULL,
  `league_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `league_iconUrls_tiny` varchar(256) CHARACTER SET utf8 NOT NULL,
  `league_iconUrls_small` varchar(256) CHARACTER SET utf8 NOT NULL,
  `league_iconUrls_medium` varchar(256) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`league_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
