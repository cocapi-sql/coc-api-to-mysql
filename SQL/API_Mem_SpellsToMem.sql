--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Table structure for table `API_Mem_SpellsToMem`
--

CREATE TABLE IF NOT EXISTS `API_Mem_SpellsToMem` (
  `spellsToMem_id` int(10) NOT NULL AUTO_INCREMENT,
  `spellsToMem_member_tag` varchar(20) NOT NULL,
  `spellsToMem_spells_name` varchar(20) NOT NULL,
  `spellsToMem_timestamp` datetime NOT NULL,
  `spellsToMem_level` int(2) NOT NULL,
  PRIMARY KEY (`spellsToMem_id`),
  KEY `spellsToMem_member_tag` (`spellsToMem_member_tag`,`spellsToMem_spells_name`),
  KEY `spellsToMem_spells_name` (`spellsToMem_spells_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

--
-- Constraints for table `API_Mem_SpellsToMem`
--
ALTER TABLE `API_Mem_SpellsToMem`
  ADD CONSTRAINT `API_Mem_SpellsToMem_ibfk_1` FOREIGN KEY (`spellsToMem_member_tag`) REFERENCES `API_Member` (`member_tag`),
  ADD CONSTRAINT `API_Mem_SpellsToMem_ibfk_2` FOREIGN KEY (`spellsToMem_spells_name`) REFERENCES `API_Mem_Spells` (`spells_name`);
