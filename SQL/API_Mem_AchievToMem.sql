--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Table structure for table `API_Mem_AchievToMem`
--

CREATE TABLE IF NOT EXISTS `API_Mem_AchievToMem` (
  `achievToMem_id` int(10) NOT NULL AUTO_INCREMENT,
  `achievToMem_member_tag` varchar(20) CHARACTER SET utf8 NOT NULL,
  `achievToMem_achiev_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `achievToMem_timestamp` datetime NOT NULL,
  `achievToMem_stars` int(1) NOT NULL,
  `achievToMem_value` int(12) NOT NULL,
  `achievToMem_target` int(10) NOT NULL,
  `achievToMem_completionInfo` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`achievToMem_id`),
  KEY `achievToMem_member_tag` (`achievToMem_member_tag`),
  KEY `achievToMem_achiev_name` (`achievToMem_achiev_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

--
-- Constraints for table `API_Mem_AchievToMem`
--
ALTER TABLE `API_Mem_AchievToMem`
  ADD CONSTRAINT `API_Mem_AchievToMem_ibfk_1` FOREIGN KEY (`achievToMem_member_tag`) REFERENCES `API_Member` (`member_tag`),
  ADD CONSTRAINT `API_Mem_AchievToMem_ibfk_2` FOREIGN KEY (`achievToMem_achiev_name`) REFERENCES `API_Mem_Achiev` (`achiev_name`);

