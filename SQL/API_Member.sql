--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------
-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Table structure for table `API_Member`
--

CREATE TABLE IF NOT EXISTS `API_Member` (
  `member_id` int(10) NOT NULL AUTO_INCREMENT,
  `member_tag` varchar(20) CHARACTER SET utf8 NOT NULL,
  `member_clan_tag` varchar(20) CHARACTER SET utf8 NOT NULL,
  `member_league_id` int(10) NOT NULL,
  `member_name` varchar(40) CHARACTER SET utf16 NOT NULL,
  `member_timestamp` datetime NOT NULL,
  `member_clanRank` int(2) NOT NULL,
  `member_previousClanRank` int(2) NOT NULL,
  `member_expLevel` int(4) NOT NULL,
  `member_role` varchar(10) CHARACTER SET utf8 NOT NULL,
  `member_townHallLevel` int(2) NOT NULL,
  `member_trophies` int(6) NOT NULL,
  `member_bestTrophies` int(6) NOT NULL,
  `member_warStars` int(6) NOT NULL,
  `member_attackWins` int(6) NOT NULL,
  `member_defenseWins` int(6) NOT NULL,
  `member_donations` int(6) NOT NULL,
  `member_total_donations` int(8) NOT NULL,
  `member_donationsReceived` int(6) NOT NULL,
  `member_total_donationsReceived` int(8) NOT NULL,
  PRIMARY KEY (`member_id`),
  KEY `member_info_tag` (`member_tag`,`member_clan_tag`,`member_league_id`),
  KEY `member_info_clan_tag` (`member_clan_tag`),
  KEY `member_info_league_id` (`member_league_id`),
  KEY `member_timestamp` (`member_timestamp`),
  KEY `member_name` (`member_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

--
-- Constraints for table `API_Member`
--
ALTER TABLE `API_Member`
  ADD CONSTRAINT `API_Member_ibfk_2` FOREIGN KEY (`member_clan_tag`) REFERENCES `API_Clan` (`clan_tag`),
  ADD CONSTRAINT `API_Member_ibfk_3` FOREIGN KEY (`member_league_id`) REFERENCES `API_League` (`league_id`);

