--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Table structure for table `API_Mem_Spells`
--

CREATE TABLE IF NOT EXISTS `API_Mem_Spells` (
  `spells_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `spells_maxLevel` int(2) NOT NULL,
  PRIMARY KEY (`spells_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
