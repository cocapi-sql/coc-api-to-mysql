-----------------------------------------------
--                  COC API                  -- 
--                Version 1.1                --
--                                           --
--     Used to pull Clash of Clans clan,     --
--    member and war log data via the API    --
--      and load this data into mySQL.       --
--                                           --
--              by Gary Douglas              --
--               Oct. 16, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Table structure for table `API_Clan`
--

CREATE TABLE IF NOT EXISTS `API_Clan` (
  `clan_tag` varchar(20) CHARACTER SET utf8 NOT NULL,
  `clan_name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `clan_description` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `clan_badgeUrls_small` varchar(256) CHARACTER SET utf8 NOT NULL,
  `clan_badgeUrls_medium` varchar(256) CHARACTER SET utf8 NOT NULL,
  `clan_badgeUrls_large` varchar(256) CHARACTER SET utf8 NOT NULL,
  `clan_location_id` int(10) NOT NULL,
  `clan_location_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `clan_location_countryCode` varchar(2) CHARACTER SET utf8 NOT NULL,
  `clan_warFrequency` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`clan_tag`),
  UNIQUE KEY `clan_tag` (`clan_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
