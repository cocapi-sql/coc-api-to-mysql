--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Table structure for table `API_Mem_Heros`
--

CREATE TABLE IF NOT EXISTS `API_Mem_Heros` (
  `hero_name` varchar(20) NOT NULL,
  `hero_maxLevel` int(3) NOT NULL,
  PRIMARY KEY (`hero_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
