--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Table structure for table `API_Mem_HerosToMem`
--

CREATE TABLE IF NOT EXISTS `API_Mem_HerosToMem` (
  `heroToMem_id` int(10) NOT NULL AUTO_INCREMENT,
  `heroToMem_member_tag` varchar(20) NOT NULL,
  `heroToMem_heros_name` varchar(20) NOT NULL,
  `heroToMem_timestamp` datetime NOT NULL,
  `heroToMem_level` int(3) NOT NULL,
  PRIMARY KEY (`heroToMem_id`),
  KEY `heroToMem_member_tag` (`heroToMem_member_tag`,`heroToMem_heros_name`),
  KEY `heroToMem_heros_name` (`heroToMem_heros_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

--
-- Constraints for table `API_Mem_HerosToMem`
--
ALTER TABLE `API_Mem_HerosToMem`
  ADD CONSTRAINT `API_Mem_HerosToMem_ibfk_1` FOREIGN KEY (`heroToMem_member_tag`) REFERENCES `API_Member` (`member_tag`),
  ADD CONSTRAINT `API_Mem_HerosToMem_ibfk_2` FOREIGN KEY (`heroToMem_heros_name`) REFERENCES `API_Mem_Heros` (`hero_name`);
