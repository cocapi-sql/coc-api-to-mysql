--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Table structure for table `API_Mem_Achiev`
--

CREATE TABLE IF NOT EXISTS `API_Mem_Achiev` (
  `achiev_name` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `achiev_info` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`achiev_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

