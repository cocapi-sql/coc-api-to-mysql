-----------------------------------------------
--                  COC API                  -- 
--                Version 1.1                --
--                                           --
--     Used to pull Clash of Clans clan,     --
--    member and war log data via the API    --
--      and load this data into mySQL.       --
--                                           --
--              by Gary Douglas              --
--               Oct. 16, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Table structure for table `API_WarLog`
--

CREATE TABLE IF NOT EXISTS `API_WarLog` (
  `warlog_id` int(10) NOT NULL AUTO_INCREMENT,
  `warlog_result` varchar(10) CHARACTER SET utf8 NOT NULL,
  `warlog_endTime` datetime NOT NULL,
  `warlog_teamSize` int(2) NOT NULL,
  `warlog_clan_tag` varchar(20) CHARACTER SET utf8 NOT NULL,
  `warlog_clan_clanLevel` int(3) NOT NULL,
  `warlog_clan_stars` int(3) NOT NULL,
  `warlog_clan_destructionPercentage` decimal(5,2) NOT NULL,
  `warlog_clan_attacks` int(3) NOT NULL,
  `warlog_clan_expEarned` int(4) NOT NULL,
  `warlog_opp_tag` varchar(20) CHARACTER SET utf8 NOT NULL,
  `warlog_opp_name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `warlog_opp_clanLevel` int(3) NOT NULL,
  `warlog_opp_stars` int(3) NOT NULL,
  `warlog_opp_destructionPercentage` decimal(5,2) NOT NULL,
  `warlog_opp_badgeUrls_small` varchar(256) CHARACTER SET utf8 NOT NULL,
  `warlog_opp_badgeUrls_medium` varchar(256) CHARACTER SET utf8 NOT NULL,
  `warlog_opp_badgeUrls_large` varchar(256) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`warlog_id`),
  KEY `warlog_clan_tag` (`warlog_clan_tag`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

--
-- Constraints for table `API_WarLog`
--
ALTER TABLE `API_WarLog`
  ADD CONSTRAINT `API_WarLog_ibfk_1` FOREIGN KEY (`warlog_clan_tag`) REFERENCES `API_Clan` (`clan_tag`);
