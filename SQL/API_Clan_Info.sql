-----------------------------------------------
--                  COC API                  -- 
--                Version 1.1                --
--                                           --
--     Used to pull Clash of Clans clan,     --
--    member and war log data via the API    --
--      and load this data into mySQL.       --
--                                           --
--              by Gary Douglas              --
--               Oct. 16, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `thebl962_jumi`
--

-- --------------------------------------------------------

--
-- Table structure for table `API_Clan_Info`
--

CREATE TABLE IF NOT EXISTS `API_Clan_Info` (
  `clan_info_id` int(20) NOT NULL AUTO_INCREMENT,
  `clan_info_time` datetime NOT NULL,
  `clan_info_tag` varchar(20) CHARACTER SET utf8 NOT NULL,
  `clan_info_clanLevel` int(3) NOT NULL,
  `clan_info_clanPoints` int(8) NOT NULL,
  `clan_info_warWins` int(4) NOT NULL,
  `clan_info_warWinStreak` int(3) NOT NULL,
  `clan_info_warTies` int(4) NOT NULL,
  `clan_info_warLosses` int(4) NOT NULL,
  `clan_info_members` int(2) NOT NULL,
  `clan_info_type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `clan_info_requiredTrophies` int(5) NOT NULL,
  `clan_info_isWarLogPublic` tinyint(1) NOT NULL,
  PRIMARY KEY (`clan_info_id`),
  KEY `clan_info_tag` (`clan_info_tag`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

--
-- Constraints for table `API_Clan_Info`
--
ALTER TABLE `API_Clan_Info`
  ADD CONSTRAINT `API_Clan_Info_ibfk_1` FOREIGN KEY (`clan_info_tag`) REFERENCES `API_Clan` (`clan_tag`);

