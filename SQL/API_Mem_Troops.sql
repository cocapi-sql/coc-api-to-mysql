--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Table structure for table `API_Mem_Troops`
--

CREATE TABLE IF NOT EXISTS `API_Mem_Troops` (
  `troops_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `troops_maxLevel` int(2) NOT NULL,
  PRIMARY KEY (`troops_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
