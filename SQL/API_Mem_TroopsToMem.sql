--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Table structure for table `API_Mem_TroopsToMem`
--

CREATE TABLE IF NOT EXISTS `API_Mem_TroopsToMem` (
  `troopsToMem_id` int(10) NOT NULL AUTO_INCREMENT,
  `troopsToMem_member_tag` varchar(20) CHARACTER SET utf8 NOT NULL,
  `troopsToMem_troops_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `troopsToMem_timestamp` datetime NOT NULL,
  `troopsToMem_level` int(2) NOT NULL,
  PRIMARY KEY (`troopsToMem_id`),
  KEY `troopsToMem_member_tag` (`troopsToMem_member_tag`),
  KEY `troopsToMem_troops_name` (`troopsToMem_troops_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

--
-- Constraints for table `API_Mem_TroopsToMem`
--
ALTER TABLE `API_Mem_TroopsToMem`
  ADD CONSTRAINT `API_Mem_TroopsToMem_ibfk_1` FOREIGN KEY (`troopsToMem_member_tag`) REFERENCES `API_Member` (`member_tag`),
  ADD CONSTRAINT `API_Mem_TroopsToMem_ibfk_2` FOREIGN KEY (`troopsToMem_troops_name`) REFERENCES `API_Mem_Troops` (`troops_name`);
