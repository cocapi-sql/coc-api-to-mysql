--                                           --
--              by Gary Douglas              --
--               Dec. 10, 2016               --
--        http://www.the-blacklist.ca        --
-----------------------------------------------
-- Used to update table from version 1.1 to version 1.2

ALTER TABLE `API_Member` 
  ADD `member_townHallLevel` int(2) NOT NULL,
  ADD `member_bestTrophies` int(6) NOT NULL,
  ADD `member_warStars` int(6) NOT NULL,
  ADD `member_attackWins` int(6) NOT NULL,
  ADD `member_defenseWins` int(6) NOT NULL;
