# README #

This README is for setting up the COC API to mySQL scripts

### What is this repository for? ###


This is a perl script to pull the Clash of Clans clan, member, and war log data via the COC API. It then takes this data and puts it in a mySQL database. This can be set up to run as a cron and keep historical data of clans.


Version 1.2.1

### How do I get set up? ###

* Summary of set up
    * A little bare here as this is my first time at something like this, hopefully you have some experience with setting this stuff up
* Configuration
    * API Token's
        * The script is setup to run with two API Tokens from SuperCell
        * https://developer.clashofclans.com/#/getting-started
        * This is so that the member token does not pull too much data and block the clan token
        * This script could possible run with same token entered twice
    * You will need to replace the items in <> in the config.pl file
        * <DB NAME> - set to your database name
        * <DB USERNAME> - set to your database username
        * <DB PASSWORD> - set to your database password
        * <CLAN API TOKEN> - set to Clan API Token
        * <MEMBER API TOKEN> - set to Member API Token 
        * <CLAN TAG> - set to the clan tag you want to pull
            * You can pull from multiple clans by repeating this line as show in the code
* Dependencies
    * PERL
    * mySQL
    * Perl [JSON::PARSE](http://search.cpan.org/~bkb/JSON-Parse-0.41/lib/JSON/Parse.pod)
* Database configuration
    * Included you will find the .sql scripts to create the databases
    * You will need to create 14 tables
    * Note: The `API_Member` table had new fields added in Version 1.2 (see API_Member_Update.sql for update)
* How to run tests
    * Debug
    	* Debug mode will print detail log information
    	* Change the `return 0;` to `return 1;` in config.pl file
    * Test
    	* Test mode will not do any sql changes
    	* Change the `return 0;` to `return 1;` in config.pl file
* Deployment instructions
    * I have this running every hour as a cron job
    * I was running this every 15 minutes and it was getting to much data that was not changing

### Version ###
* 1.0
    * Working version
* 1.1
    * Added total donations fields to member database
* 1.2
    * Moved sql statements to a separate file (API_SQL_Statements.pl)
    * Moved configuration to separate file (config.pl)
    * Update script to pull new detail member info and enter into sql
    * Note: SQL will need to be updated from 1.1 to support this new data, this includes updating the `API_Member` table with new fields
* 1.2.1
    * Fixed problem with achievement entries for new players

### Contribution guidelines ###

* Contact me

### Who do I talk to? ###

* Back End - that includes these scripts
    * Post an issue or suggestion at the [issue page](https://bitbucket.org/cocapi-sql/coc-api-to-mysql/issues?status=new&status=open)
* Front End - this includes displaying the data collected in mySQL to your web page
    * I am not going to be able to help with this, this all depends on your setup
    * I am using Joomla-Juma-Plotalot to display this information in graphs within our clan web site
    * Currently all this is private, but I might open up some samples