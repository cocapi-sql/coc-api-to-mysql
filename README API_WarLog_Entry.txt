# README #

This README is for running the API_WarLog_Entry.pl script

If you have run the API_ClanTag_cron.pl script successfully and had an war log of the clan set to public, then you have a lot of war data that is not represented in the clan_into table of the database. This script will pull the warlog table and insert this data into the clan_info table. 


Version 1.0

### How do I get set up? ###

* Summary of set up
    * Just run the script after running API_ClanTag_cron.pl script successfully and having the war log in COC set to public.
* Configuration
    * You will need to replace the items in <> at the top of the script
        * <DB NAME> - set to your database name
        * <DB USERNAME> - set to your database username
        * <DB PASSWORD> - set to your database password
        * <API TOKEN> - set to your API Token that you get from [SuperCell](https://developer.clashofclans.com/#/getting-started)
        * <CLAN TAG> - set to the clan tag you want to pull
            * You can pull from multiple clans by repeating this line as show in the code
* Dependencies
    * PERL
    * mySQL
    * API_ClanTag_cron.pl
* Database configuration
    * The API_ClanTag_cron.pl has to be run successfully for this script to work
    * Rows updated
    	* clan_info_time - uses the time from the warlog entry
    	* clan_info_tag - uses the correct clan tag
    	* clan_info_clanLevel - uses the clan level from the warlog entry
    	* clan_info_clanPoints - subtracts warlog_clan_expEarned from the previous clan info entry
    		* not the most accurate as exp is earned for other stuff, but the best I can do with the data I have
    	* clan_info_warWins - updated to represet correct number of wins
    	* clan_info_warWinStreak - update to represet correct win streak
    		* this was more difficult the the other entries :)
    	* clan_info_warTies - updated to represet correct number of ties
    	* clan_info_warLosses - updated to represet correct number of losses
    	* clan_info_members - set to 0 as this is unknown
    	* clan_info_type - set to 0 as this is unknown
    	* clan_info_requiredTrophies - set to 0 as this is unknown
    	* clan_info_isWarLogPublic - set to 0 as this is unknown
* How to run tests
	* Debug
    	* Change the `my $debug = 0;` to `my $debug = 1;` for detail debug info
* Deployment instructions
    * You should only need to run this script once per clan 

### Contribution guidelines ###

* Contact me

### Who do I talk to? ###

* Back End - that includes these scripts
    * Post an issue or suggestion at the [issue page](https://bitbucket.org/cocapi-sql/coc-api-to-mysql/issues?status=new&status=open)
* Front End - this includes displaying the data collected in mySQL to your web page
    * I am not going to be able to help with this, this all depends on your setup
    * I am using Joomla-Juma-Plotalot to display this information in graphs within our clan web site
    * Currently all this is private, but I might open up some samples