# README #

This README is for running the API_WarLog_Entry.pl script

This is to add up total donations for a database that already has been running. You will need to add the member_total_donations and member_total_donationsReceived field to your sql before running this script. This script can take a while to run if you have been collecting data for a while.


Version 1.1

### How do I get set up? ###

* Summary of set up
    * Just run the script after upgrading the database if you have been running Version 1.0.
* Configuration
    * You will need to replace the items in <> at the top of the script
        * <DB NAME> - set to your database name
        * <DB USERNAME> - set to your database username
        * <DB PASSWORD> - set to your database password
* Dependencies
    * PERL
    * mySQL
    * API_ClanTag_cron.pl
* Database configuration
    * The API_ClanTag_cron.pl has to be run successfully for this script to work
    * Rows updated
    	* member_total_donations - will add up the member_donations field
    	* member_total_donationsReceived - will add up the member_donationsReceived field
* How to run tests
	* Debug
    	* Change the `my $debug = 0;` to `my $debug = 1;` for detail debug info
* Deployment instructions
    * You should only need to run this script once 

### Contribution guidelines ###

* Contact me

### Who do I talk to? ###

* Back End - that includes these scripts
    * Post an issue or suggestion at the [issue page](https://bitbucket.org/cocapi-sql/coc-api-to-mysql/issues?status=new&status=open)
* Front End - this includes displaying the data collected in mySQL to your web page
    * I am not going to be able to help with this, this all depends on your setup
    * I am using Joomla-Juma-Plotalot to display this information in graphs within our clan web site
    * Currently all this is private, but I might open up some samples