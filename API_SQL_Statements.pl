#! /usr/bin/perl

use strict;
use warnings;

# set up warlog search sql statement
sub get_warlog_search {
	return "SELECT warlog_opp_tag  
	  FROM `API_WarLog` 
	  WHERE `warlog_clan_tag` LIKE ? 
	  ORDER BY `warlog_endTime` DESC 
	  LIMIT 1";
};

# set up warlog insert sql statement
sub get_warlog_insert {
	return "INSERT INTO `" . $dbname . "`.`API_WarLog` (`warlog_id`, `warlog_result`, 
	  `warlog_endTime`, `warlog_teamSize`, `warlog_clan_tag`, `warlog_clan_clanLevel`, `warlog_clan_stars`, 
	  `warlog_clan_destructionPercentage`, `warlog_clan_attacks`, `warlog_clan_expEarned`, `warlog_opp_tag`, 
	  `warlog_opp_name`, `warlog_opp_clanLevel`, `warlog_opp_stars`, `warlog_opp_destructionPercentage`, 
	  `warlog_opp_badgeUrls_small`, `warlog_opp_badgeUrls_medium`, `warlog_opp_badgeUrls_large`) 
	  VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
};

# set up clan info war lose search sql statement
sub get_clan_warLosses_search {
	return "SELECT `clan_info_time`, `clan_info_warTies`, `clan_info_warLosses` 
	   FROM `API_Clan_Info` 
	   WHERE `clan_info_tag` LIKE ? 
		 AND `clan_info_warLosses` > 0 
	   ORDER BY `clan_info_time` 
	   DESC LIMIT 1";
};

# set up clan search sql statement
sub get_clan_search {
	return "SELECT * 
	   FROM `API_Clan` 
	   WHERE `clan_tag` LIKE ?";
};

# set up clan insert sql statement
sub get_clan_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Clan` (`clan_tag`, `clan_name`, `clan_description`, 
	   `clan_badgeUrls_small`, `clan_badgeUrls_medium`, `clan_badgeUrls_large`, `clan_location_id`, 
	   `clan_location_name`, `clan_location_countryCode`, `clan_warFrequency`) 
	   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
};

# set up clan update sql statement
sub get_clan_update {
	return "UPDATE `" . $dbname . "`.`API_Clan` 
	  SET `clan_description` = ?,
		 `clan_badgeUrls_small` = ?,
		 `clan_badgeUrls_medium` = ?,
		 `clan_badgeUrls_large` = ?,
		 `clan_location_id` = ?,
		 `clan_location_name` = ?,
		 `clan_location_countryCode` = ?,
		 `clan_warFrequency` = ?
	  WHERE `API_Clan`.`clan_tag` = ?";
};

# set up clan info insert sql statement
sub get_clan_info_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Clan_Info` (`clan_info_id`, `clan_info_time`, 
	   `clan_info_tag`, `clan_info_clanLevel`, `clan_info_clanPoints`, `clan_info_warWins`, 
	   `clan_info_warWinStreak`,`clan_info_warTies`, `clan_info_warLosses`, `clan_info_members`, 
	   `clan_info_type`,`clan_info_requiredTrophies`, `clan_info_isWarLogPublic`) 
	   VALUES ( NULL,?,?,?,?,?,?,?,?,?,?,?,? )";
};

# set up member search sql statement
sub get_member_search {
	return "SELECT `member_donations`, `member_total_donations`, `member_donationsReceived`, 
	   `member_total_donationsReceived` 
	   FROM `API_Member` 
	   WHERE `member_tag` LIKE ? 
	   ORDER BY `member_timestamp` DESC 
	   LIMIT 1";
};

# set up member insert sql statement
sub get_member_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Member` (`member_id`, `member_tag`, 
	   `member_clan_tag`, `member_league_id`, `member_name`, `member_timestamp`, `member_clanRank`, 
	   `member_previousClanRank`, `member_expLevel`, `member_role`, `member_townHallLevel`, `member_trophies`, 
	   `member_bestTrophies`, `member_warStars`, `member_attackWins`, `member_defenseWins`, `member_donations`, 
	   `member_total_donations`, `member_donationsReceived`, `member_total_donationsReceived`) 
	   VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
};

# set up league search sql statement
sub get_league_search {
	return "SELECT *
	   FROM `API_League`
	   WHERE `league_id` =?";
};

# set up league insert sql statement
sub get_league_insert {
	return "INSERT INTO `" . $dbname . "`.`API_League` (`league_id`, `league_name`, 
	   `league_iconUrls_tiny`, `league_iconUrls_small`, `league_iconUrls_medium`) 
	   VALUES (?, ?, ?, ?, ?)";
};

# set up member achievement search sql statement
sub get_achievToMem_search {
	return "SELECT `achievToMem_achiev_name`, 
	  MAX(`achievToMem_value`) AS achievToMem_value 
	  FROM `API_Mem_AchievToMem` AS ATM 
	  WHERE `achievToMem_member_tag` LIKE ?
	  GROUP BY `achievToMem_achiev_name`";
};

# set up member achievement insert sql statement
sub get_achievToMem_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Mem_AchievToMem` (
	   `achievToMem_id` , `achievToMem_member_tag` , `achievToMem_achiev_name` , `achievToMem_timestamp` ,
	   `achievToMem_stars` , `achievToMem_value` , `achievToMem_target` , `achievToMem_completionInfo`) 
	   VALUES (NULL , ?, ?, ?, ?, ?, ?, ?)";
};

# set up achievement insert sql statement
sub get_achiev_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Mem_Achiev` (`achiev_name`, `achiev_info`) 
	   VALUES (?, ?)";
};

# set up member troop search sql statement
sub get_troopsToMem_search {
	return "SELECT `troopsToMem_troops_name`,  
	  MAX(`troopsToMem_level`) AS troopsToMem_level 
	  FROM `API_Mem_TroopsToMem` 
	  WHERE `troopsToMem_member_tag` LIKE ? 
	  GROUP BY `troopsToMem_troops_name`";
};

# set up member troop insert sql statement
sub get_troopsToMem_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Mem_TroopsToMem` (`troopsToMem_id`, 
	   `troopsToMem_member_tag`, `troopsToMem_troops_name`, `troopsToMem_timestamp`, `troopsToMem_level`) 
	   VALUES (NULL , ?, ?, ?, ?)";
};

# set up troop search sql statement
sub get_troops_search {
	return "SELECT * 
	   FROM `API_Mem_Troops`";
};

# set up troop insert sql statement
sub get_troops_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Mem_Troops` (`troops_name`, `troops_maxLevel`) 
	   VALUES (?, ?)";
};

# set up troop update sql statement
sub get_troops_update {
	return "UPDATE `" . $dbname . "`.`API_Mem_Troops` 
	  SET `troops_maxLevel` = ? 
	  WHERE `API_Mem_Troops`.`troops_name` = ?";
};

# set up member hero search sql statement
sub get_heroToMem_search {
	return "SELECT `heroToMem_heros_name`,  
	  MAX(`heroToMem_level`) AS heroToMem_level 
	  FROM `API_Mem_HerosToMem` 
	  WHERE `heroToMem_member_tag` LIKE ? 
	  GROUP BY `heroToMem_heros_name`";
};

# set up member hero insert sql statement
sub get_heroToMem_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Mem_HerosToMem` (`heroToMem_id`, 
	   `heroToMem_member_tag`, `heroToMem_heros_name`, `heroToMem_timestamp`, `heroToMem_level`) 
	   VALUES (NULL , ?, ?, ?, ?)";
};

# set up hero search sql statement
sub get_hero_search {
	return "SELECT * 
	   FROM `API_Mem_Heros`";
};

# set up hero insert sql statement
sub get_hero_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Mem_Heros` (`hero_name`, `hero_maxLevel`) 
	   VALUES (?, ?)";
};

# set up heros update sql statement
sub get_hero_update {
	return "UPDATE `" . $dbname . "`.`API_Mem_Heros` 
	  SET `hero_maxLevel` = ? 
	  WHERE `API_Mem_Heros`.`hero_name` = ?";
};

# set up member spell search sql statement
sub get_spellToMem_search {
	return "SELECT `spellsToMem_spells_name`,  
	  MAX(`spellsToMem_level`) AS spellsToMem_level 
	  FROM `API_Mem_SpellsToMem` 
	  WHERE `spellsToMem_member_tag` LIKE ? 
	  GROUP BY `spellsToMem_spells_name`";
};

# set up member spell insert sql statement
sub get_spellToMem_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Mem_SpellsToMem` (`spellsToMem_id`, 
	   `spellsToMem_member_tag`, `spellsToMem_spells_name`, `spellsToMem_timestamp`, `spellsToMem_level`) 
	   VALUES (NULL , ?, ?, ?, ?)";
};

# set up spell search sql statement
sub get_spell_search {
	return "SELECT * 
	   FROM `API_Mem_Spells`";
};

# set up spell insert sql statement
sub get_spell_insert {
	return "INSERT INTO `" . $dbname . "`.`API_Mem_Spells` (`spells_name`, `spells_maxLevel`) 
	   VALUES (?, ?)";
};

# set up spell update sql statement
sub get_spell_update {
	return "UPDATE `" . $dbname . "`.`API_Mem_Spells` 
	  SET `spells_maxLevel` = ? 
	  WHERE `API_Mem_Spells`.`spells_name` = ?";
};



return 1;
